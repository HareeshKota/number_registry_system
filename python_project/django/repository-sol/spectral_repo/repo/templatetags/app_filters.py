from django import template
from repo.models import Container,DataFile,FileType,ContainerInfo
from django.db.models import Q

register = template.Library()

@register.filter
def fields_dict(value):
     fields=value._meta.get_fields()
     return fields

@register.filter
def opposite(value):
     if value == 'desc':
       return 'asc'
     else: 
       return 'desc'

@register.filter
def getDataFiles(value):
     return value.datafile_set.filter(Q(category__name='Data') | Q(category__name='Primary Data'))

@register.filter
def getSupportDataFiles(value):
     return value.datafile_set.exclude(Q(category__name='Data') | Q(category__name='Primary Data'))

@register.filter
def getImageFiles(value):
     return value.imagefile_set.all()

from django.forms import ModelForm,HiddenInput,ModelChoiceField
from django.forms.models import modelformset_factory
from django.core.exceptions import NON_FIELD_ERRORS
from django.db.models import Q
from repo.models import Container,DataFile,ImageFile,FileCategory,FileType,ContainerInfo

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, HTML
from crispy_forms.bootstrap import FormActions

class ContainerForm(ModelForm):
    class Meta:
       model = Container
       fields = ['name']
       labels = {
        "name": "Name of the dataset"
       }
       '''widgets = {
            'container': HiddenInput(),
        }'''
       def __init__(self, *args, **kwargs):
          super(ContainerForm, self).__init__(*args, **kwargs)
          self.helper = FormHelper(self)
          #self.helper.form_id = 'id-ContainerForm'
          #self.helper.form_class = 'blueForms'
          #form_method = 'post'
          #form_id = 'container-form-id'
          #form_show_errors = True
          #render_hidden_fields = False
          self.helper.layout.append(
            FormActions(
                HTML("""<a role="button" class="btn btn-default"
                        href="{% url "some_cancel_url" %}">Cancel</a>"""),
                Submit('save', 'Submit'),
            ))

class DataFileForm(ModelForm):
    class Meta:
       model = DataFile
       fields = ['dataFile']
       labels = {
        "dataFile" : "Data File"
        }
       def __init__(self, *args, **kwargs):
          super(DataFileForm, self).__init__(*args, **kwargs)
          self.helper.layout.append(
            FormActions(
                HTML("""<a role="button" class="btn btn-default"
                        href="{% url "some_cancel_url" %}">Cancel</a>"""),
                Submit('save', 'Submit'),
            )) 

class SupportDataFileForm(ModelForm):
    class Meta:
       model = DataFile
       fields = ['name','dataFile','comments']
       labels = {
        "name": "Document Name",
        "dataFile": "Supporting File",
        "comments": "Additional Comments"
       }
       def __init__(self, *args, **kwargs):
          super(SupportDataFileForm, self).__init__(*args, **kwargs)
          #self.fields['category'].queryset = FileCategory.objects.filter(id=1) 

class ImageFileForm(ModelForm):
    class Meta:
       model = ImageFile
       fields = ['name','image','comments']
       labels = {
        "name": "Image Name",
        "image": "Image File",
        "comments": "Additional Comments"
       }
       def __init__(self, *args, **kwargs):
          super(ImageFileForm, self).__init__(*args, **kwargs)
          self.helper.layout.append(
            FormActions(
                HTML("""<a role="button" class="btn btn-default"
                        href="{% url "some_cancel_url" %}">Cancel</a>"""),
                Submit('save', 'Submit'),
            ))


class ContainerInfoForm(ModelForm):
    '''container = ModelChoiceField(queryset=Container.objects,empty_label=None)'''
    '''def __init__(self,user,*args,**kwargs):
       super (ContainerInfoForm,self ).__init__(*args,**kwargs)
       self.fields['container'].queryset = Container.objects.filter(owner=user)'''

    class Meta:
       model = ContainerInfo
       fields = ['attrA', 'attrB', 'attrC', 'attrD', 'comments']
       error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "%(field_labels)s are not unique.",
            }
        }
       def __init__(self, *args, **kwargs):
          super(ContainerForm, self).__init__(*args, **kwargs)
          self.helper = FormHelper()
          self.helper.form_id = 'id-ContainerInfoForm'
          self.helper.form_class = 'blueForms'
          self.helper.layout.append(Submit('save', 'save'))

from django.db import models

import datetime
from django.utils import timezone

from site_utils import checksum
from django.contrib.auth import models as django_models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from site_utils import validator

def upload_location(instance, filename):
    return '{0}/{1}'.format(instance.container.id,filename)

class Container(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20,blank=False,null=False)
    created = models.DateTimeField(auto_now_add=True,blank=True)
    modified = models.DateTimeField(auto_now=True,blank=True)
    owner = models.ForeignKey(django_models.User,default=1,on_delete=models.SET_DEFAULT,blank=False,null=False)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return "/container/%s/" %(self.id)
    def get_delete_url(self):
        return "/container/%s/delete/" %(self.id)
    def was_created_recently(self):
        return self.created >= timezone.now() - datetime.timedelta(days=1)
    class Meta:
        permissions = (
            ("read", "Can view contents of a container"),
        )
        unique_together = (("owner", "name"),)
        ordering = ['id']

class DataFile(models.Model):
    id = models.AutoField(primary_key=True)
    dataFile = models.FileField(upload_to=upload_location,validators=[validator.validate_file_extension],blank=False,null=False)
    name = models.CharField(max_length=30,blank=False,null=False)
    comments = models.TextField(max_length=300,blank=True,null=True)
    fileType = models.ForeignKey('FileType',default=1,on_delete=models.SET_DEFAULT,blank=False,null=False)
    container = models.ForeignKey('Container',on_delete=models.CASCADE,blank=False,null=False)
    created = models.DateTimeField(auto_now_add=True,blank=True)
    modified = models.DateTimeField(auto_now=True,blank=True)
    category = models.ForeignKey('FileCategory',default=1,on_delete=models.SET_DEFAULT,blank=False,null=False)
    def __str__(self):
        return self.dataFile.name
    def filepath(self):
        pass
    def unkown_filetype(self):
        pass
    class Meta:
        permissions = (
            ("read", "Can read a datafile"),
        )

class ImageFile(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30,blank=False,null=False)
    comments = models.TextField(max_length=300,blank=True,null=True)
    image = models.ImageField(upload_to=upload_location,validators=[validator.validate_image_extension],blank=False,null=False)
    imageType = models.ForeignKey('FileType',default=1,on_delete=models.SET_DEFAULT,blank=False,null=False)
    container = models.ForeignKey('Container',on_delete=models.CASCADE,blank=False,null=False)
    created = models.DateTimeField(auto_now_add=True,blank=True)
    modified = models.DateTimeField(auto_now=True,blank=True)
    def __str__(self):
        return self.image.name
    def filepath(self):
        pass
    def unkown_filetype(self):
        pass
    class Meta:
        permissions = (
            ("read", "Can read a datafile"),
        )

class FileCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30,blank=False,null=False)
    def __str__(self):
        return self.name

class FileChecksum(models.Model):
    md5sum = models.CharField(max_length=32,blank=False,null=False)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    def __str__(self):
        return self.md5sum

class Checksum(models.Model):
    contentfile = models.OneToOneField('DataFile',null=False,on_delete=models.CASCADE,primary_key=True)
    md5sum = models.CharField(max_length=32,blank=False,null=False)
    def __str__(self):
        return self.md5sum

class FileType(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30,blank=True,null=True)
    extension = models.CharField(max_length=5,blank=False,null=False)
    def __str__(self):
        return self.name

class ContainerInfo(models.Model):
    container = models.OneToOneField('Container',null=False,on_delete=models.CASCADE,primary_key=True)
    attrA = models.CharField(max_length=30,blank=True,null=True)
    attrB = models.CharField(max_length=30,blank=True,null=True)
    attrC = models.CharField(max_length=30,blank=True,null=True)
    attrD = models.CharField(max_length=30,blank=True,null=True)
    comments = models.TextField(max_length=300,blank=True,null=True)
    def __str__(self):
        return self.container
    def get_absolute_url(self):
        return "/container/%s/" %(self.id)
    class Meta:
        permissions = (
            ("update", "Can update a metadata attribute"),
        )


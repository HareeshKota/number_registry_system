# -*- coding: utf-8 -*-
'''
- This module contains view methods and supportive methods. Each view method is responsible 
to render a specific URL or URL pattern. We can also used Class-based views for the same
purpose, but we have used function-based views anyway as I am more convenient with them.
https://docs.djangoproject.com/en/2.0/topics/http/views/
- Throughout the module, we have different view methods for each URL/URLpattern and in each 
of those we will be doing both or either of two kind of actions: 
1. Backround Processing 2. Rendering Webpage 
- During background processing, we will be creating or updating model instances or retrieving 
info from models and then, we render the web page or redirect to a different URL. 
https://docs.djangoproject.com/en/2.0/topics/db/models/
- For creating or updating model instance, we may use inputs from form or we give our own or 
derive inputs. 
- Manytimes we save model instance without commiting to the database. We will commit to db 
only after updating all required fields of the model. Otherwise, db won't allow the update.
- We make the use of templates where we take care of the display formatting of the web page. 
https://docs.djangoproject.com/en/2.0/topics/templates/
- We add @login_required decorator just before few views to automatically redirect it's page to a 
login page if a user is not logged in. LOGIN_URL should be set in the settings for this to work. 
https://docs.djangoproject.com/en/2.0/topics/auth/default/#the-login-required-decorator
- We make the user of modelforms instead of normal forms 
https://docs.djangoproject.com/en/2.0/topics/forms/modelforms/

The following are the different view methods:

'''
from django.shortcuts import get_object_or_404,render
from django.http import HttpResponseRedirect,HttpResponse
from django.core.paginator import Paginator
from django.core.files import File
#from django.core import serializers
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.urls import reverse
#from django.template import loader
from django.db.models import Q
from django.db.models.fields.files import FieldFile
from django.conf import settings

from repo.models import Container,DataFile,ImageFile,FileChecksum,Checksum,FileType,FileCategory,ContainerInfo
from repo.forms import ContainerForm,DataFileForm,ContainerInfoForm,SupportDataFileForm,ImageFileForm

from site_utils import checksum
import os

@login_required
def addContainer(request):
    '''
    View method for creating new container and add data file.
    
    The following is how it does it:
       - A new "Container" object is created with "Name" input of the form 
         which it obtains through ContainerForm(request.POST).
       - Then, it calls "addFile" method to create a new "DataFile" object 
         with relation to the Container object and with "Spectral Data" input 
         obtained through DataFileForm(request.FILES). 
       - Then, it creates a new empty "ContainerInfo" object with relation to 
         "Container" object.
       - Then, it redirects webpage to 'edit_container_info' url allowing user 
         to update the container info.
    '''
   
    if request.method == 'POST':
        #If a form is submitted, this will get the inputs from form and process them.
        c_inst = ContainerForm(request.POST)
        if c_inst.is_valid():
            #Container object creation starts
            new_c = c_inst.save(commit=False)
            new_c.owner = request.user
            new_c.save()
            messages.success(request, "New container is created with id: {0}".format(new_c.id)) #to display result message to end user
            #ends
            
            #DataFile object creation starts
            category = FileCategory.objects.get(name="Primary Data") 
            addFile(request,'DataFile',new_c,category) 
            #ends

            #ContainerInfo object creation starts
            c_id = new_c.id
            new_ci = ContainerInfo(container=new_c)
            new_ci.save()
            messages.success(request, "New container info is created with id: {0}".format(new_c.id))
            #ends
            
            #Redirecting to edit_container_info to update container info. Parameter is the newly container id.
            return HttpResponseRedirect(reverse('edit_container_info',kwargs={'id':c_id}))
    else:
        #If a form is not submitted, this will render the form. 
        #Here we use two modelforms in single form
        c_form = [ ContainerForm(), DataFileForm() ]  
        context = { 'forms' : c_form, 'nbar' : 'add', 'title': 'Create Container' }
        return render(request, 'form.html', context)

@login_required
def addSupportFiles(request,id):
    '''
    View method to add supporting files like PDF, Images etc., but not 
    data files which is used to generate reports/graphs.
    - It takes container id as one of the paramters to which the support files are related 
    - It redirects to show_container url to show container info including the newly added support files.
    '''
    container = get_object_or_404(Container,id=id)
    if request.user in [container.owner,'admin']:
        if request.method == 'POST':
            df_inst = SupportDataFileForm(request.POST,request.FILES)
            #category = FileCategory.objects.get(id=request.POST['category'])
            category = FileCategory.objects.get(id=1)
            addFile(request,'SupportDataFile',container,category)
            return HttpResponseRedirect(reverse('show_container',kwargs={'id':id}))
        else:
            #If a form is not submitted, this will render the form.
            c_form = [ SupportDataFileForm()]
            context = { 'forms' : c_form, 'nbar' : 'add', 'title': 'Add Supporting Files' }
            return render(request, 'form.html', context)
    else:
        return render(request, 'error.html')

@login_required
def addImageFiles(request,id):
    '''
    View method to add supporting files like PDF, Images etc., but not
    data files which is used to generate reports/graphs.
    - It takes container id as one of the paramters to which the support files are related
    - It redirects to show_container url to show container info including the newly added support files.
    '''
    container = get_object_or_404(Container,id=id)
    if request.user in [container.owner,'admin']:
        if request.method == 'POST':
            im_inst = ImageFileForm(request.POST,request.FILES)
            if im_inst.is_valid():
                new_im = im_inst.save(commit=False)
                new_im.container = container
                new_im.save()
                messages.success(request, "Image is uploaded to container with id: {0}".format(container))
                ext = getFileExt(new_im.image.name)
                new_im.fileType = ext
                new_im.save()
                addChecksum2(new_im.id,'imagefile')
            return HttpResponseRedirect(reverse('show_container',kwargs={'id':id}))
        else:
            #If a form is not submitted, this will render the form.
            c_form = [ ImageFileForm() ]
            context = { 'forms' : c_form, 'nbar' : 'add', 'title': 'Add Supporting Images' }
            return render(request, 'form.html', context)
    else:
        return render(request, 'error.html')

def addFile(request,datafile,container,category):
    '''
    Supporting method to add a new file to "DataFile" model.
    - It gets called from a view method or other supporting methods while adding any files.
    - After adding a file, this method also calculate checksum of the file and save it to 
      a new instance of "Checksum" model with relation to the DataFile instance.
    - If the file belongs to 'Primary Data' category, then it generates files in other 
      formats like csv.
    - It also uses getFileExt method to obtain extension of the file.
    '''
    if datafile == "DataFile":
        df_inst = DataFileForm(request.POST,request.FILES)
    elif datafile == "SupportDataFile":
        df_inst = SupportDataFileForm(request.POST,request.FILES)
    if df_inst.is_valid():
        new_df = df_inst.save(commit=False)
        try: 
           new_df.name = request.POST['name']
        except NameError:
           new_df.name = os.path.basename(new_df.dataFile.name)
        new_df.container = container
        new_df.category = category
        new_df.save()
        messages.success(request, "File is uploaded to container with id: {0}".format(container))
        ext = getFileExt(new_df.dataFile.name)
        new_df.fileType = ext
        new_df.save()
        addChecksum2(new_df.id,'datafile')
        if new_df.category.name == 'Primary Data':
          #We will only generate files for known extensions
          known_exts = [ '.asd' ]
          if ext.extension in known_exts: 
             generateFiles(container,new_df)

def addChecksum(df):
    '''
    Supporting method to calculate checksum of a file and update as a new instance in Checksum model
    '''
    new_ck = Checksum(contentfile=df)
    new_ck.md5sum = checksum.hash_file(os.path.join(settings.MEDIA_ROOT, df.dataFile.path),'md5')
    new_ck.save()

def getFileExt(filepath):
    '''
    Supporting method to get file extension of a given file
    '''
    file_ext = os.path.splitext(filepath)[1]
    #TODO Add code to check extension using OS commands
    try:
        ext = FileType.objects.get(extension=file_ext)
    except FileType.DoesNotExist:
        ext = FileType(extension=file_ext)
        ext.save()
    return ext

def generateFiles(container,df):
     '''
     Supporting method to generate the given datafile in other formats and add them in "DataFile" model. 
     It also calculate checksum of the newly added files and updates in "Checksum" model.
     It takes container as one of the parameter to create the new file in relation to the container
     and primary data file as other parameter from where it reads data for generating the new files. 
     '''
     from site_utils import asdreader
     import csv
     #Generating CSV file
     filepath = df.dataFile.path
     filename = os.path.basename(filepath)

     #Currently we know how to read .asd files only 
     if os.path.splitext(filepath)[1]  == '.asd':
        s = asdreader.reader(filepath)
        fileprefix = os.path.splitext(filename)[0]
        #The newly generated csv file will also have same name and located at same place as of .asd
        csvname = os.path.join(fileprefix+'.csv')
        try:
          from StringIO import StringIO
        except ImportError:
          from io import StringIO
        mem_file = StringIO()
        csvwriter = csv.writer(mem_file, lineterminator='\n')
        csvwriter.writerow(["Wavelengths","Reflectance","Spec"])
        csvwriter.writerows(zip(s.wavelengths,s.reflectance,s.spec))
        newfile = File(mem_file)
        new_df = DataFile(container=container,category=FileCategory.objects.get(name='Data'),fileType=FileType.objects.get(name='CSV'))
        new_df.name = fileprefix
        new_df.dataFile.save(csvname, newfile)
        addChecksum(new_df)
     else:
        pass

def addChecksum2(id,source):
     '''
     Another method to add checksum and is currently not used. This can be used in future
     if required.
     '''
     if source == 'datafile':
        sourcefile = DataFile.objects.get(id=id)
        md5sum = checksum.hash_file(os.path.join(settings.MEDIA_ROOT, sourcefile.dataFile.path),'md5')
     if source == 'imagefile':
        sourcefile = ImageFile.objects.get(id=id)
        md5sum = checksum.hash_file(os.path.join(settings.MEDIA_ROOT, sourcefile.image.path),'md5')  
     ck = FileChecksum(content_object=sourcefile, md5sum=md5sum)
     ck.save()

@login_required
def editContainerInfo(request,id):
    '''
    View method to display and edit additional container info.
    The page is displayed only to the owners of the container or the admin
    - It redirects to show_container to display the container info including the latest updated info.
    '''
    container = get_object_or_404(Container,id=id)
    if request.user in [container.owner,'admin']: 
       container_info = get_object_or_404(ContainerInfo,container=container)
       if request.method == 'POST':
           ci_form = ContainerInfoForm(request.POST,instance=container_info)
           if ci_form.is_valid():
               updated_ci = ci_form.save(commit=False)
               updated_ci.save()
               return HttpResponseRedirect(reverse('show_container',kwargs={'id':id}))
       else:
           ci_form = [ ContainerInfoForm(instance=container_info) ]
           context =  {'forms': ci_form, 'nbar': 'edit', 'title': 'Edit Container Info'}
           return render(request, 'form.html', context)
    else:
      return render(request, 'error.html') 

@login_required
def editContainer(request):
    pass

def listContainers(request):
     '''
     View method to list all containers and to sort or search for a specific container with keywords.
     - One click Delete link is enabled only for owner of the container.
     '''
     containers = Container.objects.all()
     owner = request.GET.get("owner")
     name = request.GET.get("name")
     ordering = request.GET.get("order_by")
     direction = request.GET.get("direction")
     if direction == 'desc':
        ordering = '-{}'.format(ordering)
     if owner or name:
       containers = containers.filter(Q(owner__username__icontains=owner) & Q(name__icontains=name)).distinct()
       query1 = "&owner="+str(owner)+"&name="+str(name)
     else:
       query1 = ""
     if ordering or direction:
       containers = containers.order_by(ordering)
       query2 = "&order_by="+str(ordering)
     else:
       query2 = ""
     paginator = Paginator(containers, 5)
     page = request.GET.get('page', 1)
     try:
        containers = paginator.page(page)
     except PageNotAnInteger:
        containers = paginator.page(1)
     except EmptyPage:
        containers = paginator.page(paginator.num_pages)
    
     context = {'containers': containers, 'nbar': 'containers', 'query1': query1, 'query2': query2, 'direction': direction}
     return render(request, 'containers.html', context)

def showContainer(request,id):
     '''
     View method to show complete container info including graph and attached files.
     '''
     #container = serializers.serialize("python",get_object_or_404(Container,id=id))
     #container =  serializers.serialize("python",Container.objects.filter(id=id))[0]
     #container_info = serializers.serialize("python",ContainerInfo.objects.filter(container_id=id))[0]
     #container_info = get_object_or_404(ContainerInfo,container=container)
     container = get_object_or_404(Container,id=id)
     
     #We will read data from csv file for generating graph as mostly it will take lesser time to read from csv than asd counterpart. 
     datafile = DataFile.objects.get(Q(category__name="Data") | Q(category__name="Primary Data"),container=container,fileType__extension='.csv')
     import sys
     import pandas
     import matplotlib
     matplotlib.use('Agg')
     import matplotlib.pyplot as plt
     import numpy
     import mpld3
     from mpld3 import plugins

     sourcecsv =  datafile.dataFile.path

     #We read the whole csv file into a pandas dataframe
     data = pandas.read_csv(sourcecsv)
     properties = [('reflectance',data.Reflectance),('spec',data.Spec)]
     fig, ax = plt.subplots()
     ax.grid(True,alpha=0.3,fillstyle='full')
     for prop in properties:
        l, = ax.plot(data.Wavelengths,prop[1],label=prop[0],marker='.')
        ax.fill_between(
           data.Wavelengths,
           prop[1] * .5, prop[1] * 1.5,
           color=l.get_color(), alpha=.4
        )
     handles, labels = ax.get_legend_handles_labels() # return lines and labels
     interactive_legend = plugins.InteractiveLegendPlugin(zip(handles,ax.collections),
                                                     labels,
                                                     alpha_unsel=0,
                                                     start_visible=True)
     labels = [ "<table class='tooltip-graph'><tr><td class='prop'>Wavelength</td><td class='value'>"+str(x)+"</td></tr><tr><td class='prop'></td><td class='value'>"+str(y)+"</td></tr></table>" for x,y in l.get_xydata() ]
     tooltip = plugins.PointHTMLTooltip(l,labels=labels)
     plugins.connect(fig,interactive_legend,tooltip)
     ax.set_xlabel('Wavelength')
     ax.set_ylabel('Reflectance')
     ax.set_title(datafile.dataFile.url, size=15)
     fig_html = mpld3.fig_to_html(fig,no_extras=False,template_type="general") 
     table_html = data.to_html() 
     related_object_type = ContentType.objects.get_for_model(datafile)
     try:
       checksum = FileChecksum.objects.get(content_type__pk=related_object_type.id,object_id=datafile.id).md5sum
     except FileChecksum.DoesNotExist:
       checksum = ''
     #checksum = Checksum.objects.get(contentfile=datafile)
     context = {'container': container, 'checksum':checksum, 'nbar': 'show', 'figure': fig_html, 'table': table_html}
     return render(request, 'container.html', context)

@login_required
def deleteContainer(request,id):
     '''
     View method to delete a container and all other dependent objects
     - It redirects to list_containers url to show list of all containers.
     '''
     container = get_object_or_404(Container,id=id)
     if request.user in [container.owner,'admin']:
        container.delete()
        messages.success(request, "Container with id: {0} is successfully deleted".format(id))
        return HttpResponseRedirect(reverse('list_containers'))
     else:
        return render(request, 'error.html')

def listSupportFiles(request):
    pass

@login_required
def replaceSupportFiles(request):
    pass

@login_required
def deleteSupportFiles(request):
    pass

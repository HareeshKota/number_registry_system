def validate_file_extension(value):
    """
    To allow only specific file extensions
    """
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf', '.doc', '.docx', '.xml', '.xlsx', '.xls', '.txt', '.asd', '.csv', '.html']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported file extension.')

def validate_image_extension(value):
    """
    To allow only specific image extensions
    """
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.jpg', '.png']
    if not ext.lower() in valid_extensions:
        raise ValidationError(u'Unsupported image extension.')

import sys
import hashlib

def hash_file(path, algorithm='md5', bufsize=8192):
    h = hashlib.new(algorithm)
    with open(path, 'rb') as f:
       for chunk in iter(lambda: f.read(bufsize), b""):
          h.update(chunk)
    return h.hexdigest()

"""spectral_repo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.views.generic.base import TemplateView
from django.conf import settings
from django.conf.urls.static import static
#from django.contrib.auth import views as auth_view

#from auth.views import SignUp
from repo.views import form

urlpatterns = [
    path('', TemplateView.as_view(template_name='home.html'), name='home'), 
    path('admin/', admin.site.urls),
    path('containers/', form.listContainers, name='list_containers'),
    path('container/add/', form.addContainer, name='add_container'),
    path('container/<int:id>/', form.showContainer, name='show_container'),
    path('container/<int:id>/edit/', form.editContainer, name='edit_container'),
    path('container/<int:id>/delete/', form.deleteContainer, name='delete_container'),
    path('container/<int:id>/info/', form.editContainerInfo, name='edit_container_info'),
    path('files/', form.listSupportFiles, name='list_support_files'),
    path('container/<int:id>/files/add/', form.addSupportFiles, name='add_support_files'),
    path('container/<int:id>/images/add/', form.addImageFiles, name='add_image_files'),
    path('files/<int:id>/delete/',form.deleteSupportFiles, name='delete_support_files'),
    path('files/<int:id>/replace/', form.replaceSupportFiles, name='replace_support_files'),
    path('accounts/', include('registration.backends.default.urls')),
#    path('accounts/', include('django.contrib.auth.urls')),
#    path('accounts/signup/', SignUp.as_view(), name='signup'),
#    path('login/', auth_view.login, {'template_name': 'login.html'}, name='login'),
#    path('logout/', auth_view.logout, {'template_name': 'logout.html'}, name='logout'),
#    path('oauth/', include('social_django.urls', namespace='social')), 
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

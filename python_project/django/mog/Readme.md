 - To reset database, run the following commands:

   ```find ${APP_ROOT} -path "*/migrations/*.py" -not -name "__init__.py" -delete```
 
   ```find ${APP_ROOT} -path "*/migrations/*.pyc"  -delete```
   
   ```rm -f {APP_ROOT}/db.sqlite3```
   
 - Initialize data
   
   ```python manage.py makemigrations```
   
   ```python manage.py migrate```
   
   ```python manage.py loaddata fixtures/activties.json```


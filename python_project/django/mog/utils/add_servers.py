import django
django.setup()
import sys
serverfile = sys.argv[0]

from mog.models import Server
servers = Server.objects.only("shortname")
with open(serverfile,'rb') as f:
  for name in f:
     sh = name.split('.')[0]
     if servers.filter(shortname=sh) is None:
        new_server = Server(name=name,shortname=sh)
        new_server.save()
     else:
        print('{0} exist'.format(sh))

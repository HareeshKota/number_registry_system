from django import template
from mog.models import *
from django.shortcuts import get_object_or_404

register = template.Library()

@register.filter
def getSR(value):
   return value.serverrole_set.all()

@register.filter
def showOnly(value,activity):
   activity = get_object_or_404(Activity,name=activity)
   return value.filter(activity=activity)

@register.filter
def getModelType(value):
    if hasattr(value, 'taskurl'):
      return 'url'
    elif hasattr(value, 'tasktext'):
      return 'text'

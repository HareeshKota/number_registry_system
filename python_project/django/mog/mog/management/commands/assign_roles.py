from django.core.management.base import BaseCommand, CommandError
from django.db import IntegrityError
#from django.core.exceptions import ObjectDoesNotExist
from django.db.models.base import ObjectDoesNotExist
from mog.models import Server,Role,ServerRole

class Command(BaseCommand):
   help = 'Import servers from CSV file'

   def add_arguments(self, parser):
      parser.add_argument('csv')  
      parser.add_argument('role')

   def handle(self, *args, **options):
      serverfile = options['csv']
      role_name = options['role']
      with open(serverfile,'r') as f:
         for name in f:
           sh = name.split('.')[0]
           try:
             server = Server.objects.get(shortname=sh)
           except Server.DoesNotExist as ne:
             print("{0} is not found in Server entries".format(sh))
             continue 
           role = Role.objects.get(name=role_name)
           new_sr = ServerRole(server=server,role=role)
           try:
             new_sr.save()
           except IntegrityError as ie:
             print("{0} already added to Role".format(sh))

from django.core.management.base import BaseCommand, CommandError
from mog.models import Server

class Command(BaseCommand):
   help = 'Import servers from CSV file'

   def add_arguments(self, parser):
      parser.add_argument('csv')  

   def handle(self, *args, **options):
      serverfile = options['csv']
      servers = Server.objects.only("shortname")
      with open(serverfile,'r') as f:
         for name in f:
           sh = name.split('.')[0]
           if len(servers.filter(shortname=sh)) == 0:
              new_server = Server(name=name,shortname=sh)
              new_server.save()

from django.forms import ModelForm,HiddenInput,ModelChoiceField
from django.forms.models import modelformset_factory
from django.core.exceptions import NON_FIELD_ERRORS
from django.db.models import Q
from mog.models import *
from django.contrib.admin.widgets import FilteredSelectMultiple
from django import forms

class SanityTaskForm(ModelForm):
    class Meta:
       model = SanityTask
       exclude = ['']

class ServerForm(ModelForm):
    class Meta:
       model = Server
       exclude = ['shortname']

class RoleForm(ModelForm):
    class Meta:
       model = Role
       exclude = ['']

class ServerRoleForm(ModelForm):
    class Meta:
       model = ServerRole
       exclude = ['']

class TaskURLForm(ModelForm):
    class Meta:
       model = TaskURL
       exclude = ['']

class TaskTextForm(ModelForm):
    class Meta:
       model = TaskText
       exclude = ['']

class ServersToRoleForm(ModelForm):
    server=forms.ModelMultipleChoiceField(
                           Server.objects.all(),
                           widget=FilteredSelectMultiple("Server",False,attrs={'rows':'20'})
                                         )
    class Meta:
        model= ServerRole
        exclude = ['']

class RolesToServerForm(ModelForm):
    roles=forms.ModelMultipleChoiceField(
                           Role.objects.all(),
                           widget=FilteredSelectMultiple("Role",False,attrs={'rows':'20'})
                                         )
    class Meta:
        model= ServerRole
        exclude = ['']

from django.shortcuts import get_object_or_404,render
from django.http import HttpResponseRedirect,HttpResponse
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.db.models import Q
from django.contrib import messages
from django.urls import reverse

from itertools import chain

from mog.modelforms import *
from mog.models import *

def paginate(request,objs):
    paginator = Paginator(objs, 50)
    page = request.GET.get('page', 1)
    try:
      objs = paginator.page(page)
    except PageNotAnInteger:
      objs = paginator.page(1)
    except EmptyPage:
      objs = paginator.page(paginator.num_pages)
    return objs

def addServer(request):
    if request.method == 'POST':
      server_inst = ServerForm(request.POST)
      if server_inst.is_valid():
        new_server = server_inst.save(commit=False)
        new_server.shortname = new_server.name.split('.')[0]
        new_server.save()
        return HttpResponseRedirect(reverse('list_servers')) 
    else:
      form = ServerForm()
      context = { 'form' : form, 'title': 'Add Server' }
      return render(request, 'form.html', context)

def listServers(request):
    addServer(request)
    servers = Server.objects.all()
    name = request.GET.get("name")
    if name:
      servers = servers.filter(Q(name__icontains=name)).distinct()
      query1 = "&name="+str(name)
    else:
      query1 = ""
    servers = paginate(request,servers)
    form = ServerForm()
    context = { 'servers': servers, 'form' : form, 'title': 'Servers' }
    return render(request, 'servers.html', context)

def showServer(request):
    pass

def addRole(request):
    if request.method == 'POST':
      role_inst = RoleForm(request.POST)
      if role_inst.is_valid():
        new_role = role_inst.save(commit=False)
        new_role.save()
        return HttpResponseRedirect(reverse('list_roles'))
    else:
      form = RoleForm()
      context = { 'form' : form, 'title': 'Add Role' }
      return render(request, 'form.html', context)

def showRole(request):
    pass

def listRoles(request):
    addRole(request)
    roles = Role.objects.all()
    name = request.GET.get("name")
    if name:
      roles = roles.filter(Q(name__icontains=name)).distinct()
      query1 = "&name="+str(name)
    else:
      query1 = ""
    roles = paginate(request,roles)
    form = RoleForm()
    context = { 'roles': roles, 'form' : form, 'title': 'Roles' }
    return render(request, 'roles.html', context)

def addServerRole(request):
    if request.method == 'POST':
      sr_inst = ServerRoleForm(request.POST)
      if sr_inst.is_valid():
        new_sr = sr_inst.save(commit=False)
        new_sr.save()
        return HttpResponseRedirect(reverse('list_roles'))
    else:
      form = ServerRoleForm()
      context = { 'form' : form, 'title': 'Add Server Role' }
      return render(request, 'form.html', context)

def addServersToRole(request):
    if request.method == 'POST':
      sr_inst = ServersToRoleForm(request.POST)
      if sr_inst.is_valid():
        new_sr = sr_inst.save(commit=False)
        new_sr.save()
        return HttpResponseRedirect(reverse('list_roles'))
    else:
      form = ServersToRoleForm()
      context = { 'form' : form, 'title': 'Add Server Role' }
      return render(request, 'servers_role.html', context)

def listServerRoles(request):
    addServerRole(request)
    srs = ServerRole.objects.all()
    server = request.GET.get("server")
    role = request.GET.get("role")
    if server or role:
      srs = srs.filter(Q(server__name__icontains=server) & Q(role__name__icontains=role)).distinct()
      query1 = "&server="+str(server)+"&role="+str(role)
    else:
      query1 = ""
    srs = paginate(request,srs)
    form = ServerRoleForm()
    context = { 'srs': srs, 'form' : form, 'title': 'Roles', 'query1': query1 }
    return render(request, 'srs.html', context)

def listTasks(request,form=None):
    tasks = Task.objects.all()
    name = request.GET.get("name")
    text = request.GET.get("text")
    url = request.GET.get("url")
    if text or url or name:
      tasks = tasks.filter(Q(name__icontains=name) & Q(tasktext__text__icontains=text)).distinct()
      query1 = "&name="+str(name)+"&text="+str(text)+"&url="+str(url)
    else:
      query1 = ""
    tasks = paginate(request,tasks)
    context = { 'tasks': tasks, 'form' : form, 'title': 'Tasks' }
    return render(request, 'add_task.html', context)

def addTaskURL(request):
    if request.method == 'POST': 
       task_inst = TaskURLForm(request.POST,request.FILES)
       if task_inst.is_valid():
          new_task = task_inst.save(commit=False)
          new_task.save()
    form = TaskURLForm()
    return listTasks(request,form)

def addTaskText(request):
    if request.method == 'POST':
       task_inst = TaskTextForm(request.POST,request.FILES)
       if task_inst.is_valid():
          new_task = task_inst.save(commit=False)
          new_task.save()
    form = TaskTextForm()
    return listTasks(request,form)

def editTask(request,id):
    task = get_object_or_404(Task,id=id)
    if request.method == 'POST':
       if hasattr(task, 'taskurl'):
          task_inst = TaskURLForm(request.POST,request.FILES)
       elif hasattr(task, 'tasktext'):
          task_inst = TaskTextForm(request.POST,request.FILES)
       if task_inst.is_valid():
          new_task = task_inst.save(commit=False)
          new_task.save()
          return HttpResponseRedirect(reverse('show_task',kwargs={'id':id}))
    else:
       if hasattr(task, 'taskurl'):
          form = TaskURLForm(instance=task)
       elif hasattr(task, 'tasktext'): 
          form = TaskTextForm(instance=task)
       context = { 'form' : form, 'title': 'Edit Task' }
       return render(request, 'edit_task.html', context)

def showTask(request,id):
    task = get_object_or_404(Task,id=id)
    context = { 'task': task}
    return render(request, 'task.html', context)

def addSanityTask(request):
    if request.method == 'POST':
      st_inst = SanityTaskForm(request.POST)
      if st_inst.is_valid():
        new_st = st_inst.save(commit=False)
        new_st.save()
        return HttpResponseRedirect(reverse('list_mogs'))
    else:
      form = SanityTaskForm()
      context = { 'form' : form, 'title': 'Add SanityTask' }
      return render(request, 'form.html', context)

def showSanityTask(request,id):
    pass

def listSanityTasks(request,id):
    pass 

def showMOG(request,id):
    if Server.objects.filter(id=id).count() == 1:
       obj = get_object_or_404(ServerOrRole,id=id)
       mappings = obj.server.serverrole_set.all()
       rl = []
       for mapping in mappings: 
          rl.append(mapping.role)
       sts = SanityTask.objects.filter(obj__in=rl+[obj])
    elif Role.objects.filter(id=id).count() == 1:
       obj = get_object_or_404(ServerOrRole,id=id)
       sts = SanityTask.objects.filter(obj=obj)
    name = obj.name
    activities = Activity.objects.all()
    context = { 'sts': sts, 'activities': activities, 'title': 'MOG', 'name': name }
    return render(request, 'mog.html', context)

def listMOGs(request):
    objs = ServerOrRole.objects.all()
    name = request.GET.get("name")
    if name:
      objs = objs.filter(Q(name__icontains=name)).distinct()
      query1 = "&name="+str(name)
    else:
      query1 = ""
    objs = paginate(request,objs)
    context = { 'objs': objs, 'title': 'MOGs List' }
    return render(request, 'mogs.html', context)

def deleteRole(request,id):
    get_object_or_404(Role,id=id).delete()
    messages.success(request, "Role with id: {0} is successfully deleted".format(id))
    return HttpResponseRedirect(reverse('list_roles'))

def deleteServer(request,id):
    get_object_or_404(Server,id=id).delete()
    messages.success(request, "Server with id: {0} is successfully deleted".format(id))
    return HttpResponseRedirect(reverse('list_servers'))

def deleteServerRole(request,id):
    get_object_or_404(ServerRole,id=id).delete()
    messages.success(request, "ServerRole with id: {0} is successfully deleted".format(id))
    return HttpResponseRedirect(reverse('list_srs'))

def deleteSanityTask(request,id):
    get_object_or_404(SanityTask,id=id).delete()
    messages.success(request, "SanityTask with id: {0} is successfully deleted".format(id))
    return HttpResponseRedirect(reverse('list_mogs'))

def deleteTask(request,id):
    get_object_or_404(Task,id=id).delete()
    messages.success(request, "Task with id: {0} is successfully deleted".format(id))
    return HttpResponseRedirect(reverse('list_tasks'))

from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
#from tinymce.models import HTMLField
#from quill.fields import RichTextField
from ckeditor.fields import RichTextField

class ServerOrRole(models.Model):
   id = models.AutoField(primary_key=True)
   created = models.DateTimeField(auto_now_add=True,blank=True)
   modified = models.DateTimeField(auto_now=True,blank=True)
   name = models.CharField(max_length=50,blank=False,null=False,unique=True)
   def __str__(self):
      return self.name
   class Meta:
      ordering = ['id']

class Server(ServerOrRole):
   shortname = models.CharField(max_length=20,blank=True,null=False,unique=True)

class Role(ServerOrRole):
   pass

class ServerRole(models.Model):
   id = models.AutoField(primary_key=True)
   server = models.ForeignKey('Server',on_delete=models.CASCADE,blank=False,null=False)
   role = models.ForeignKey('Role',on_delete=models.CASCADE,blank=False,null=False)
   def __str__(self):
      return "Server: {0}, Role: {1}".format(self.server,self.role)
   class Meta:
      unique_together = (("server", "role"),)
      ordering = ['id']

class Task(models.Model):
   id = models.AutoField(primary_key=True)
   name = models.CharField(max_length=80,blank=True,null=True)
   created = models.DateTimeField(auto_now_add=True,blank=True)
   modified = models.DateTimeField(auto_now=True,blank=True)
   def __str__(self):
      return self.name
   class Meta:
      ordering = ['id']

class TaskURL(Task):
   url = models.URLField(blank=False,null=False) 

class TaskText(Task):
   text = RichTextField(config_name='basic_ckeditor')

class Activity(models.Model):
   id =  models.AutoField(primary_key=True)
   name = models.CharField(max_length=10,blank=False,null=False)
   def __str__(self):
      return self.name

class SanityTask(models.Model):
   id =  models.AutoField(primary_key=True)
   obj = models.ForeignKey('ServerOrRole',on_delete=models.CASCADE,blank=False,null=False)
   activity = models.ForeignKey('Activity',on_delete=models.CASCADE,blank=False,null=False)
   stask = models.ForeignKey('Task',on_delete=models.CASCADE,blank=False,null=False)
   comments = models.TextField(max_length=300,blank=True,null=True)
   def __str__(self):
      return self.id
   class Meta:
      ordering = ['id']

"""hweetest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include,reverse,reverse_lazy
from django.views.generic.base import RedirectView
from django.conf import settings
from django.conf.urls.static import static
from mog import views as mog_views
import ckeditor_uploader

urlpatterns = [
   # path('admin/',name="add_server"),
    path('', RedirectView.as_view(url='servers/')),
    path('server/add/', mog_views.addServer, name="add_server"),
    path('server/<int:id>/', mog_views.showServer, name="show_server"),
    path('servers/', mog_views.listServers, name="list_servers"),
    path('role/add/', mog_views.addRole, name="add_role"),
    path('role/<int:id>/', mog_views.showRole, name="show_role"),
    path('roles/', mog_views.listRoles, name="list_roles"),
    path('task/url/add/', mog_views.addTaskURL, name="add_task_url"),
    path('task/text/add/', mog_views.addTaskText, name="add_task_text"),
    path('task/<int:id>/', mog_views.showTask, name="show_task"),
    path('task/<int:id>/edit/', mog_views.editTask, name="edit_task"),
    path('tasks/', mog_views.listTasks, name="list_tasks"),
    path('sanity_task/add/', mog_views.addSanityTask, name="add_st"),
    path('sanity_task/<int:id>/', mog_views.showSanityTask, name="show_st"),
    path('sanity_tasks/', mog_views.listSanityTasks, name="list_sts"),
    path('server_role/add/', mog_views.addServerRole, name="add_sr"),
    path('server_roles/', mog_views.listServerRoles, name="list_srs"),
    path('role/<int:id>/delete/', mog_views.deleteRole, name="delete_role"),
    path('server/<int:id>/delete/', mog_views.deleteServer, name="delete_server"),
    path('server_role/<int:id>/delete/', mog_views.deleteServerRole, name="delete_sr"),
    path('task/<int:id>/delete/', mog_views.deleteTask, name="delete_task"),
    path('sanity_task/<int:id>/delete/', mog_views.deleteSanityTask, name="delete_st"),
    path('roles/servers/add/', mog_views.addServersToRole, name="severs_role"),
    path('mog/<int:id>/', mog_views.showMOG, name="show_mog"),
    path('mogs/', mog_views.listMOGs, name="list_mogs"),
    path('ckeditor/', include('ckeditor_uploader.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

from django import forms
#from django.core.validators import RegexValidator
#from django.core.exceptions import ValidationError
import re

class SearchForm(forms.Form):
    pattern = forms.CharField(
                 label='Search Number',
                 max_length=10)
    def clean_pattern(self):
       pattern = self.cleaned_data['pattern']
       if not re.match("([*0-9]){10}",pattern) and not re.match("([0-9]){1,9}",pattern):
         raise forms.ValidationError('Only numbers and asthetics are allowed')
       return pattern

class ReserveForm(forms.Form):
    number = forms.CharField(
                 label='Reserve Number',
                 min_length=10,
                 max_length=10)
    def clean_number(self):
       number = self.cleaned_data['number']
       if not re.match("([0-9]){10}",number):
         raise forms.ValidationError('Only numbers are allowed')
       return number

class DetailsForm(forms.Form):
    number = forms.CharField(
                 label='Number',
                 min_length=10,
                 max_length=10)
    def clean_number(self):
       number = self.cleaned_data['number']
       if not re.match("([0-9]){10}",number):
         raise forms.ValidationError('Only numbers are allowed')
       return number

class RegisterForm(forms.Form):
    number = forms.CharField(
                 label='Register Number',
                 min_length=10,
                 max_length=10)
    def clean_number(self):
       number = self.cleaned_data['number']
       if not re.match("([0-9]){10}",number):
         raise forms.ValidationError('Only numbers are allowed')
       return number

class LoginForm(forms.Form):
    username = forms.CharField(label='Username',max_length=15)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)
    superuser = forms.BooleanField(label='Login as Service Provider',initial=False, required=False)

class SignupForm(forms.Form):
    username = forms.CharField(label='Username',max_length=15)
    password = forms.CharField(max_length=32, widget=forms.PasswordInput)
    confirm_password = forms.CharField(max_length=32, widget=forms.PasswordInput)
    firstname = forms.CharField(label='First Name',max_length=15)
    lastname = forms.CharField(label='Last Name',max_length=15)
    email = forms.EmailField(max_length=254,label='Email ID') 
    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        if password != confirm_password:
            raise forms.ValidationError(
                "password and confirm_password does not match"
            )
    



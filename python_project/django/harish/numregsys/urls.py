from django.urls import path
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('login/', views.login, name='login'),
    path('search/', views.search, name='search'),
    path('reserve/', views.reserve, name='reserve'),
    path('register/', views.register, name='register'),
    path('details/', views.details, name='details'),
#    path('delete/', views.delete, name='delete'),
    path('logout/', views.logout, name='logout'),
    path('signup/', views.signup, name='signup')
]

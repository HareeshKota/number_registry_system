from django.shortcuts import get_object_or_404,render
from django.http import HttpResponseRedirect,HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt,requires_csrf_token,csrf_protect

from . import forms as myForms

wso2server = "http://127.0.0.1:8280"

@csrf_exempt
def login(request):
   username=''
   if request.method == 'POST':
      form = myForms.LoginForm(request.POST)
      if form.is_valid():
         username = form.cleaned_data['username']
         password = form.cleaned_data['password']
         superuser = form.cleaned_data['superuser']
         token,scope=tokenRequest(username,password)
         if superuser is True and scope == "default":
             token = ""
             message = "You are not a Service Provider!"
         if token != "":
           request.session['username'] = username
           request.session['token'] = token
           return HttpResponseRedirect(reverse('welcome'))
         else:
           message = "Username or password is incorrect. Please try again!"
           form = myForms.LoginForm()
           return render(request, 'login.html', {'title': 'Login', 'form': form, 'message': message})
   form = myForms.LoginForm()
   return render(request, 'login.html', {'title': 'Login', 'form': form})

def logout(request):
   token = request.session['token']
   revokeRequest(token)
   del request.session['username']
   del request.session['token']
   return HttpResponseRedirect(reverse('login'))

def signup(request):
   if request.method == 'POST':
      form = myForms.SignupForm(request.POST)
      if form.is_valid():
         username = form.cleaned_data['username']
         password = form.cleaned_data['password']
         firstname = form.cleaned_data['firstname']
         lastname = form.cleaned_data['lastname']
         email = form.cleaned_data['email']
         message = signupRequest(username,password,firstname,lastname,email)
         if message == "":
           message = "Signup successful for user: "+username
           return HttpResponseRedirect(reverse('login'))
         return render(request, 'signup.html', {'title': 'Sign Up', 'form': form, 'message': message})
   form = myForms.SignupForm()
   return render(request, 'signup.html', {'title': 'Sign Up', 'form': form})

@csrf_exempt
def signupRequest(username,password,firstname,lastname,email):
  import requests
  import json
  url='http://localhost:9763/store/site/blocks/user/sign-up/ajax/user-add.jag'
  headers = {
    'Accept': 'application/json',
  }
  payload = {
   'action' : 'addUser',
   'username' : username,
   'password' : password,
   'allFieldsValues' : firstname+'|'+lastname+'||||'+email
  }
  response = requests.post(url, headers=headers, data=payload)
  jsonout = json.loads(response.text)
  if 'error' in jsonout:
    if jsonout['error'] is False:
      return ""
    return "Signup failed: "+jsonout['message']
  return "Signup failed. Unkown error"

@csrf_exempt
def tokenRequest(username,password):
  import requests
  import json
  url = wso2server+"/token"
  headers = {
    'Accept': 'application/json',
    'Accept-Encoding': 'gzip, deflate',
    'Authorization': 'Basic VHdsOUVXV3NaNHRLTTlfMnN5YVZLR2JZRTlJYToyZnJYWVk1VzJocmRyTm9XTTJNQ043eGJvN3dh',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Content-Length': '59',
    'Content-Type': 'application/x-www-form-urlencoded',
    'cache-control': 'no-cache',
  }
  payload = {
    'grant_type': 'password',
    'scope': 'write_data',
    'username': username,
    'password': password
  }
  response = requests.post(url, headers=headers, data=payload)
  jsonout = json.loads(response.text)
  if 'access_token' in jsonout:
    return jsonout['access_token'], jsonout['scope']
  else:
    return "", ""

@csrf_exempt
def revokeRequest(token):
  import requests
  import json
  url = wso2server+"/revoke"
  headers = {
    'Accept': 'application/json',
    'Accept-Encoding': 'gzip, deflate',
    'Authorization': 'Basic VHdsOUVXV3NaNHRLTTlfMnN5YVZLR2JZRTlJYToyZnJYWVk1VzJocmRyTm9XTTJNQ043eGJvN3dh',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Content-Length': '59',
    'Content-Type': 'application/x-www-form-urlencoded',
    'cache-control': 'no-cache',
  }
  payload = {
    'token': token
  }
  requests.post(url, headers=headers, data=payload)

@csrf_exempt
def searchRequest(pattern,token):
  import requests
  import json
  url = wso2server+"/v1/num/search"
  payload = "{\n\"pattern\":\""+pattern+"\"\n}"
  headers = {
    'Content-Type': "application/json",
    'token': "fdsg",
    'Accept': "application/json",
    'Cache-Control': "no-cache",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "26",
    'Connection': "keep-alive",
    'cache-control': "no-cache",
    'Authorization' : "Bearer "+token
  }
  response = requests.request("PUT", url, data=payload, headers=headers)
  jsonout = json.loads(response.text)
  if jsonout['numList'][0] == "":
    return jsonout['numList'],"No numbers found for the given pattern"
  return jsonout['numList'],""

@csrf_exempt
def registerRequest(number,token):
  import requests
  import json
  url = wso2server+"/v1/num/register"
  payload = "{\"num\":\""+number+"\"}"
  headers = {
    'Content-Type': "application/json",
    'token': "fdsg",
    'Accept': "application/json",
    'Cache-Control': "no-cache",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "26",
    'Connection': "keep-alive",
    'cache-control': "no-cache",
    'Authorization' : "Bearer "+token
  }
  response = requests.request("POST", url, data=payload, headers=headers)
  jsonout = json.loads(response.text)
  if 'num' in jsonout:
    message = "Registration of number "+number+" is successful"
    return message
  elif 'error' in jsonout:
    message = jsonout['error']
    return message
  elif 'fault' in jsonout:
    message = jsonout['fault']['message']
    return message

@csrf_exempt
def reserveRequest(number,token):
  import requests
  import json
  url = wso2server+"/v1/num/reserve"
  payload = "{\"num\":\""+number+"\"}"
  headers = {
    'Content-Type': "application/json",
    'token': "fdsg",
    'Accept': "application/json",
    'Cache-Control': "no-cache",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "26",
    'Connection': "keep-alive",
    'cache-control': "no-cache",
    'Authorization' : "Bearer "+token
  }
  response = requests.request("PUT", url, data=payload, headers=headers)
  jsonout = json.loads(response.text)
  if 'status' in jsonout:
    message = "Reservation of number "+number+" is successfull"
    return message
  elif 'error' in jsonout:
    message = jsonout['error']
    return message
  else:
    message = "Reservation of number "+number+" is failed"
    return message

@csrf_exempt
def detailsRequest(number,token):
  import requests
  import json
  url = wso2server+"/v1/num/get/"+number
  headers = {
    'Content-Type': "application/json",
    'token': "fdsg",
    'Accept': "application/json",
    'Cache-Control': "no-cache",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "26",
    'Connection': "keep-alive",
    'cache-control': "no-cache",
    'Authorization' : "Bearer "+token
  }
  response = requests.request("GET", url, headers=headers)
  jsonout = json.loads(response.text)
  if 'status' in jsonout:
    return jsonout
  else:
    return []

def welcome(request):
    username = request.session.get('username')
    token = request.session.get('token')
    if token == "" or token is None:
      return HttpResponseRedirect(reverse('login'))
    return render(request, 'welcome.html', {'title': 'Home', 'username': username})

@csrf_exempt
def search(request):
    username = request.session.get('username')
    token = request.session.get('token')
    if token == "" or token is None:
      return HttpResponseRedirect(reverse('login'))
    if request.method=='POST' and 'pattern' in request.POST:
      form = myForms.SearchForm(request.POST)
      if form.is_valid():
        form = form.cleaned_data
        pattern = form.get('pattern')
        numlist,message = searchRequest(pattern,token) 
        form = myForms.SearchForm()
        return render(request, 'search.html' , {'title': 'Search', 'form': form, 'numlist': numlist, 'message': message, 'username':username})
    form = myForms.SearchForm()
    return render(request, 'search.html' , {'title': 'Search', 'form': form, 'username': username})

@csrf_exempt
def reserve(request):
    username = request.session.get('username')
    token = request.session.get('token')
    if token == "" or token is None:
      return HttpResponseRedirect(reverse('login'))
    if request.method=='POST' and 'number' in request.POST:
      form = myForms.ReserveForm(request.POST)
      if form.is_valid():
        form = form.cleaned_data
        number = form.get('number')
        message = reserveRequest(number,token) 
        form = myForms.ReserveForm()
        return render(request, 'reserve.html' , {'title': 'Reserve Number', 'form': form, 'message': message, 'username': username})
    form = myForms.ReserveForm()
    return render(request, 'reserve.html' , {'title': 'Reserve Number', 'form': form, 'username': username})

@csrf_exempt
def register(request):
    username = request.session.get('username')
    token = request.session.get('token')
    if token == "" or token is None:
      return HttpResponseRedirect(reverse('login'))
    if request.method=='POST' and 'number' in request.POST:
      form = myForms.RegisterForm(request.POST)
      if form.is_valid():
        form = form.cleaned_data
        number = form.get('number')
        message = registerRequest(number,token)
        form = myForms.RegisterForm()
        return render(request, 'register.html' , {'title': 'Register Number', 'form': form, 'message': message, 'username': username})
    form = myForms.RegisterForm()
    return render(request, 'register.html' , {'title': 'Register Number', 'form': form, 'username': username})

@csrf_exempt
def delete(request):
   pass

@csrf_exempt
def details(request):
    username = request.session.get('username')
    token = request.session.get('token')
    if token == "" or token is None:
      return HttpResponseRedirect(reverse('login'))
    if request.method=='GET' and 'number' in request.GET:
      form = myForms.DetailsForm(request.GET)
      if form.is_valid():
        form = form.cleaned_data
        number = form.get('number')
        data = detailsRequest(number,token)
        message = ""
        if not data:
          message = "Number not found"
        form = myForms.DetailsForm()
        return render(request, 'details.html' , {'title': 'Number Details', 'form': form, 'message': message, 'data': data, 'username': username})
    form = myForms.DetailsForm()
    return render(request, 'details.html' , {'title': 'Number Details', 'form': form, 'username': username})

 - To add empty directories, run this command:
   find $PATH_TO_REPOSITORY -type d ! -path "*.git*" -empty -exec cp dummy-gitignore '{}'/.gitignore \;
 - To ignore __pycache__ directories, run this command:
   find $PATH_TO_REPOSITORY -type d ! -path "*.git*" -name '__pycache__' -exec cp pycache-gitignore '{}'/.gitignore \;
